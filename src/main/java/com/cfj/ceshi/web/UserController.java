package com.cfj.ceshi.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cfj.ceshi.entity.UserEntity;
import com.cfj.ceshi.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	//按名字查询
	@RequestMapping("/getUserByName")
	public String getUserByName(String name) {
		UserEntity user= userService.findUserByUserName(name);
		return user.toString();
	}
	
	//增加方法
	@RequestMapping("/add")
	public void save() {
		 UserEntity user=new UserEntity();
	     user.setId(3l);
	     user.setUserName("小明");
	     user.setPassWord("fffooo123");
	     userService.saveUser(user);
	 }
	 //更新方法
	 @RequestMapping("/update")
	 public void update(UserEntity user) {
		 userService.updateUser(user);
	 }
	 //删除方法
	 @RequestMapping(value="/delete")
	 public void delete(Long id) {
		 userService.deleteUserById(id);
	 }
    

}
