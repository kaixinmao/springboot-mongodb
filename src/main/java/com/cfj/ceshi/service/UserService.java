package com.cfj.ceshi.service;

import com.cfj.ceshi.entity.UserEntity;

public interface UserService {
	
	public void saveUser(UserEntity user);

    public UserEntity findUserByUserName(String userName);

    public int updateUser(UserEntity user);

    public void deleteUserById(Long id);
   
}
