package com.cfj.ceshi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfj.ceshi.dao.UserDao;
import com.cfj.ceshi.entity.UserEntity;
import com.cfj.ceshi.service.UserService;


@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
    private UserDao userDao;

	@Override
	public void saveUser(UserEntity user) {
		userDao.saveUser(user);
	}

	@Override
	public UserEntity findUserByUserName(String userName) {
		return userDao.findUserByUserName(userName);
	}

	@Override
	public int updateUser(UserEntity user) {
		return userDao.updateUser(user);
	}

	@Override
	public void deleteUserById(Long id) {
		userDao.deleteUserById(id);
	}
	

}
